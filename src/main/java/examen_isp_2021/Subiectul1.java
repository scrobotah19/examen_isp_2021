package examen_isp_2021;



public class Subiectul1 {

    class B{
        private String param;

        public void x(){}
        public void y(){}

        D d;

        E e;

        B(E e) {
            this.e = e;
        }
    }

    class A extends B{

        A(E e) {
            super(e);
        }
    }

    class D{

    }

    class C{

        B bList = new B();

    }

    class E{

    }

    class U{

        U(B b){
            System.out.println(b);
        }

    }
}
