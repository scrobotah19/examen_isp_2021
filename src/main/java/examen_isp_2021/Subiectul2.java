package examen_isp_2021;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiectul2 extends JFrame {

    JTextArea tArea1, tArea2;
    JButton button;

    Subiectul2() {
        setTitle("Numar litere");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        tArea1 = new JTextArea();
        tArea1.setBounds(10, 10, 150, 80);

        tArea2 = new JTextArea();
        tArea2.setBounds(10, 100, 150, 80);

        button = new JButton("Press It");
        button.setBounds(40, 190, width, height);

        button.addActionListener(new TratareButon());

        add(button);
        add(tArea1);
        add(tArea2);
    }

    public static void main(String[] args) {
        new Subiectul2();
    }
    class TratareButon implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String prop;
            int car=0,j=0;
            String textnou = "";

            prop=tArea1.getText();

            for(int i = 0; i < prop.length(); i++) {
                if(prop.charAt(i) != ' ')
                    car++;
            }

            for(j=0;j<car;j++){

                textnou=textnou+"a";
            }
            tArea2.setText(textnou);


        }
    }
}
